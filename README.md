![](https://i.imgur.com/wh2QXAt.png)


## Avalith Front End Development Challenge

Welcome! Thanks in advance for the time and interest in joining the Avalith team. Here you'll find a challenge specifically oriented to showcase some of your coding (and maybe other) skills while working with a similar set of tools to the ones we use on a daily basis, in an intuitive and design-based approach. 
We think that this is somewhat more pragmatic and spontaneous than getting you to code live on a website with a clock ticking ⏱ (and probably you do too).


### Overview

The goal of the assignment is to create a Movie Search [Single Page Application](https://es.wikipedia.org/wiki/Single-page_application) that consists of two main screens:

* A **Search screen** with a list of movies pulled from an API.
* A **Movie Detail screen** showcasing a selected movie and some of its relevant information / metadata.

A set of mockups can be found at the following link for reference:

[InVision Prototype](https://projects.invisionapp.com/d/main?origin=v7#/console/18597465/386791440/preview)
Note: You will need an InVision account. Once created, send a request for access.

In order to speed things up data-wise to focus on the UI, here are a few request examples that should get you up and running. We're using the [OMDB API](http://www.omdbapi.com/), but you're free to use any source of data you prefer, as long as it's not static.

#### GET movies by title search ####

`t`: movie title

`y`: year

Example:

```bash
http://www.omdbapi.com/?t=wolf+of&y=2013
```

#### GET movie information by IMDB ID (full plot) ####

`i`: id

`plot`: 'full' | 'short'

Example:

```bash
http://www.omdbapi.com/?i=tt0993846&plot=full
```

Keep in mind these requests need to be suffixed with an API key you can very easily get, along with further information, at the [OMDB website ](http://www.omdbapi.com/). For example.

```bash
http://www.omdbapi.com/?s=wolf&y=2013&apiKey=<your-api-key>
```

### Tech Stack & Requirements

At Avalith, our main stack is currently based on **React & Redux** 💎, but ultimately, feel free to use any set of technologies you feel comfortable / most proficient with, such as Angular, VueJS, or a jQuery solution (preferably not the latter 😄). You'll most likely be called in for a brief showcase of your final output and walk through some technical decisions. Keep that in mind when thinking of implementing utility libraries such as Lodash or Ramda (that's fine as long as you can explain what you're doing afterwards).

SASS, LESS and CSS in JS are welcome as well.

(It's your party, try to keep it modern) 🤘

### What we are looking for

We pay special attention to:

- A basic README file with information about the project (e.g. how to install dependencies, how to run it locally). Make sure these commands actually work! You'd be amazed at how many previous challenges didn't get past the **yarn start** phase.
- How stylesheets and class names are structured. Is there any special methodology? ([BEM](http://getbem.com/introduction/) is your friend). Is it reusable? Scalable?
- Code structure (modularity, dependencies, scaffolding, documentation, etc)
- Semantic HTML and whether it's being used properly along with CSS & JavaScript
- It would be great if search inputs are **debounced** or **validated with at least 3 characters** (maybe both?) 🤓
- Discussions are encouraged. We chat regularly with other engineers and design teams as part of the daily workflow. Don't hesitate on asking questions as we're going to check if things work as the requirements describe after all.
- A good eye for design and overall feeling of the application UI wise. That's what the Figma specs are for. Try to match it as much as possible, making every pixel count 🙌
- State management, if any libraries or patters are used, readability, usage of modern front end technologies and best practices.
- Edge cases (think about the _loading_ search state, for example. What happens if no movies match a particular search term? And if a user gets into a movie detail page _by route_?) These are all very important on a day-to-day project workflow.
- [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself), [SOLID](https://en.wikipedia.org/wiki/SOLID), [KISS](https://en.wikipedia.org/wiki/KISS_principle) and [YAGNI](https://en.wikipedia.org/wiki/You_aren%27t_gonna_need_it), if you find them applicable (can't go wrong with any of them)

### Bonus points
If you fancy doing a little extra, here are some things you could spend your time on:

- **Animations**, as it's 2019 already! Page transitions / hover states are always a plus.
- **Responsiveness**. If your app is mobile friendly. No specs are provided so flex your creative muscles if you can! 💻
- **Deployment**. Hosting on _**Heroku**_ or _**Netlify**_ is very much apprecciated and saves everyone's time (It's also terribly easy nowadays)

We're aware that there's no way to deliver a completely polished application in a day, or getting into the nitty gritty details of how your tools of choice work during an interview. You have stuff going on in your life, so focus on the skills that best demonstrate your skill and passion. Please try not to worry about setting up a node server to pull things from as it falls out of this particular scope.

### Delivery

Once you're happy with the results, send us a .zip file to any of the following emails:

- [martinm@avalith.net](mailto:martinm@avalith.net)
- [mariel@avalith.net](mailto:mariel@avalith.net)

Please attach a link to the hosted heroku / netlify / firebase website if there is one!

### That's all!

Happy hacking 🎉 

![Happy Hacking](https://media.tenor.com/images/df8c44a1d20ab367fdcb21880985fd33/tenor.gif)